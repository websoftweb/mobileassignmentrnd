//
//  Created by Ebenezer Gabriels on 11/4/18.
//  Copyright © 2018 Ebenezer Gabriels. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    //the approach is to set up tables and views progrmatically
    //what you code is what you get
    private var cellId = "citiesTableCell"
    private var cellHeight = CGFloat(70)
    private var cellTitleDisplayConjunction = ", "
    private var citiesTableView: UITableView!
    private var currentViewFrame: CGSize!
    private var dataFileName = "cities"
    private var dataFileNameExtension = "json"
    private var loaderBgViewAlpha = CGFloat(0.7)
    private var loaderImageView: UIImageView!
    private var loaderImageViewImage = "launch_bg_2.png"
    private var loaderText = "Just a moment please, \nI am loading city details. "
    private var loaderTextFontSize = CGFloat(24)
    
    //create searchcontroller
    private var searchController = UISearchController(searchResultsController: nil)
    private var searchControllerPlaceHolder = "Search city"
    //loadingview setup
    private var setupLoadingLabel: UILabel!
    private var setupLoadingView: UIView!
    private var setupLoadingViewBG: UIView!

    private var zeroFloat = CGFloat(0)
    private var zeroInt = 0
    
    //creating the needed variable arrays to hold default data from file, sorted data and filtered data
    //an array here because i can do fast enumerations like filters, sorting, mapping and so on
    //without having to write my own loop that will traverse all the items in the set.
    //Another way to have dealt with this initial loading time would have been to segregate the data by country
    //creating different files
    //Also persisting this in the database could decrease the latency
    //This is really slow on startup despite the fact that there are skeletal views implemented
    
    var cities = [Cities]()
    var filteredCities = [Cities]()
    var sortedCities = [Cities]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get the view frame for UI setup
        currentViewFrame = self.view.frame.size
        
        //call staller/loading information view with skeletal mock view image
        considerShowingLoaderSetup(show: true)
        
        //setup search controller
        searchController.searchResultsUpdater = self as UISearchResultsUpdating
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = true
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.placeholder = searchControllerPlaceHolder
        searchController.searchBar.showsScopeBar = true
        searchController.searchBar.tintColor = .white
        definesPresentationContext = true
        navigationItem.searchController = searchController
        
        //a call to create the tableview
        createMyTable()
        
        //a call to process data from json file in the background thread
        DispatchQueue.global(qos: .background).async {
            self.processJson()
        }
    }
    
    func createMyTable () {
        //programatically create  UITableView, set its frame, datasource, datadelegate and register cell
        citiesTableView = UITableView(frame: CGRect(x: zeroFloat, y: zeroFloat, width: currentViewFrame.width, height: currentViewFrame.height))
        citiesTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        citiesTableView.dataSource = self
        citiesTableView.delegate = self
        self.view.addSubview(citiesTableView)
        self.view.sendSubviewToBack(citiesTableView)
        citiesTableView.register(CityCell.self, forCellReuseIdentifier: cellId)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredCities.count
        }
        return sortedCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CityCell
        var currentCity = sortedCities[indexPath.row]
        if isFiltering() {
            currentCity = filteredCities[indexPath.row]
        }
        cell.selectionStyle = .none
        cell.city = currentCity
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedCity = sortedCities[indexPath.row]
        if isFiltering() {
            selectedCity = filteredCities[indexPath.row]
        }
        //create details viewcontroller const
        let detailsViewController = DetailsViewController(nibName: nil, bundle: nil)
        detailsViewController.city = [selectedCity]
        //set the title of the view
        detailsViewController.title = selectedCity.name! + cellTitleDisplayConjunction + selectedCity.country!
        //navigate/push details view controller to the stack
        navigationController?.pushViewController(detailsViewController, animated: true)
    }

    func processJson() {
        let url = Bundle.main.url(forResource: dataFileName, withExtension: dataFileNameExtension)!
        do {
            let jsonData = try Data(contentsOf: url)
            
            //json decode data into the cities array ready for advanced filter map or other operations
            cities = try JSONDecoder().decode(Array<Cities>.self, from: jsonData )
            //set alphabetical sort result into the sortedCities array
            sortedCities = cities.sorted(by: { $0.name!.compare($1.name!) == .orderedAscending})
            
            reloadCitiesTable()
            considerShowingLoaderSetup(show: false)
        }
        catch {
            print(error)
        }
    }
    
    func reloadCitiesTable() {
        // might need to call reload tableview a few times on the main thread
        //so a function for it...
        DispatchQueue.main.async {
            self.citiesTableView.reloadData()
        }
    }

    //implement search bar and filtering
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        if searchAlgorithm(searchText: searchText) {
            reloadCitiesTable()
        }
        reloadCitiesTable()
    }
    
    func searchAlgorithm(searchText: String) -> Bool {
        filteredCities = cities.filter({ (city: Cities) -> Bool in
            return (city.name?.lowercased().hasPrefix(searchText.lowercased()))!
        })
        
        if filteredCities.count > zeroInt {
            return true
        }
        return false;
    }
    
    func considerShowingLoaderSetup(show: Bool) {
        if show {
            //disable search controller searchbar
            searchController.searchBar.isUserInteractionEnabled = false
            
            setupLoadingView = UIView(frame: CGRect(x: zeroFloat, y: zeroFloat, width: currentViewFrame.width, height: currentViewFrame.height))
            setupLoadingView.backgroundColor = UIColor.white
            self.view.addSubview(setupLoadingView)
            
            //load skeletal mock image
            loaderImageView = UIImageView(frame: CGRect(x: zeroFloat, y: zeroFloat, width: currentViewFrame.width, height: currentViewFrame.height))
            loaderImageView.image = UIImage(named: loaderImageViewImage)
            loaderImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
            loaderImageView.contentMode = .scaleAspectFit // OR .scaleAspectFill
            loaderImageView.clipsToBounds = true
            self.view.addSubview(loaderImageView)
            
            //load setup dark background view with 0.7 alpha
            setupLoadingViewBG = UIView(frame: CGRect(x: zeroFloat, y: zeroFloat, width: currentViewFrame.width, height: currentViewFrame.height))
            setupLoadingViewBG.backgroundColor = .black
            setupLoadingViewBG.alpha = loaderBgViewAlpha
            self.view.addSubview(setupLoadingViewBG)
            
            //setup loading text to enhance the experience
            //at least they know what is going on
            setupLoadingLabel = UILabel(frame: CGRect(x: zeroFloat, y: zeroFloat, width: currentViewFrame.width, height: currentViewFrame.height))
            setupLoadingLabel.text = loaderText
            setupLoadingLabel.textColor = .white
            setupLoadingLabel.textAlignment = .center
            setupLoadingLabel.numberOfLines = zeroInt
            setupLoadingLabel.font = UIFont.systemFont(ofSize: loaderTextFontSize)
            self.view.addSubview(setupLoadingLabel)
            
        } else {
            //conduct ui operation on main thread
            DispatchQueue.main.async {
                //remove loader items from super view
                self.setupLoadingView.removeFromSuperview()
                self.setupLoadingViewBG.removeFromSuperview()
                self.loaderImageView.removeFromSuperview()
                self.setupLoadingLabel.removeFromSuperview()
                //re-enable search controller search bar
                self.searchController.searchBar.isUserInteractionEnabled = true
            }
        }
        
    }

}



