//
//  Created by Ebenezer Gabriels on 11/5/18.
//  Copyright © 2018 Ebenezer Gabriels. All rights reserved.
//

import UIKit
import MapKit

class customMapPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    
    init(pinTitle:String, location:CLLocationCoordinate2D) {
        self.coordinate = location
        self.title = pinTitle
    }
}
