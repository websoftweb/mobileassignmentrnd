//
//  Created by Ebenezer Gabriels on 11/4/18.
//  Copyright © 2018 Ebenezer Gabriels. All rights reserved.
//

import UIKit
import MapKit


class DetailsViewController: UIViewController {
    var city = [Cities]()
    private var currentViewFrame: CGSize!
    let mapUnkownTitleKey = "UNKNOWN"
    var mapView: MKMapView?
    private var zeroFloat = CGFloat(0)
    private var zeroInt = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentViewFrame = self.view.frame.size
        mapView = MKMapView(frame: CGRect(x: zeroFloat, y: zeroFloat, width: currentViewFrame.width, height: currentViewFrame.height))
        mapView!.center = view.center
        mapView!.mapType = MKMapType.standard
        mapView!.isZoomEnabled = true
        mapView!.isScrollEnabled = true
        self.view.addSubview(mapView!)
        
        let sourceLocation = CLLocationCoordinate2D(latitude:(city[zeroInt].coord?.lat)! , longitude: (city[zeroInt].coord?.lon)!)
        
        let locationPin = customMapPin(pinTitle: city[zeroInt].name ?? mapUnkownTitleKey, location: sourceLocation)
        self.mapView!.addAnnotation(locationPin)
        mapView!.centerCoordinate = sourceLocation        
        self.view.backgroundColor = UIColor.white
    }
    
    
}
