//
//  Created by Ebenezer Gabriels on 11/4/18.
//  Copyright © 2018 Ebenezer Gabriels. All rights reserved.
//

import UIKit

struct Cities: Decodable {
    let coord: Coordinate?
    let country: String?
    let _id: Int? // id in the json really looks like an integer - at least that's the assumption
    let name: String?
}

struct Coordinate: Decodable {
    let lat: Double?
    let lon: Double?
}
