//
//  MobileAssignmentRNDTests.swift
//  MobileAssignmentRNDTests
//
//  Created by Ebenezer Gabriels on 11/4/18.
//  Copyright © 2018 Ebenezer Gabriels. All rights reserved.
//

import XCTest
@testable import MobileAssignmentRND

class MobileAssignmentRNDTests: XCTestCase {
    
    var appUndertest: ViewController!

    override func setUp() {
        appUndertest = ViewController()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSearchSuccess() {
        let coordA = Coordinate(lon: 34.283333, lat: 44.549999)
        let coordB = Coordinate(lon: 37.666668, lat: 55.683334)
        let cityA = Cities(country: "UA", name: "Hurzuf", _id: 707860, coord: coordA)
        let cityB = Cities(country: "RU", name: "Novinki", _id: 707860, coord: coordB)

        appUndertest.cities = [cityA, cityB]
        appUndertest.sortedCities = [cityA, cityB]
        let searchAlgo =  appUndertest.searchAlgorithm(searchText: "nov")
        XCTAssertTrue(searchAlgo, "valid search string with result succeeded")
    }
    
    func testSearchFailure() {
        let coordA = Coordinate(lon: 34.283333, lat: 44.549999)
        let coordB = Coordinate(lon: 37.666668, lat: 55.683334)
        let cityA = Cities(country: "UA", name: "Hurzuf", _id: 707860, coord: coordA)
        let cityB = Cities(country: "RU", name: "Novinki", _id: 707860, coord: coordB)
        
        appUndertest.cities = [cityA, cityB]
        appUndertest.sortedCities = [cityA, cityB]
        let searchAlgo =  appUndertest.searchAlgorithm(searchText: "alaba")
        XCTAssertFalse(searchAlgo, "search string with result failed")
    }

}
