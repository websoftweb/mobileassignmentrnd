//
//  Created by Ebenezer Gabriels on 11/4/18.
//  Copyright © 2018 Ebenezer Gabriels. All rights reserved.
//

import UIKit

class CityCell : UITableViewCell {
    //decided to have text label and image icons to represent the flags of the different countries
    //free image files downloaded online - free resource - just to spice the UI up a little
    
    var city : Cities? {
        didSet {
            cityLabel.text = (city?.name!)! + ", " + (city?.country!)!
            flagImage.image = UIImage(named: (city?.country?.lowercased())!)
        }
    }

    private let cityLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 18)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let flagImage : UIImageView = {
        let imgView = UIImageView(image: UIImage(named: "us"))
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(cityLabel)
        addSubview(flagImage)
        
        flagImage.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 20, paddingBottom: 5, paddingRight: 0, width: 30, height: 0, enableInsets: false)
        
        cityLabel.anchor(top: topAnchor, left: flagImage.rightAnchor, bottom: nil, right: nil, paddingTop: 25, paddingLeft: 10, paddingBottom: 30, paddingRight: 0, width: frame.size.width - 50, height: 0, enableInsets: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
